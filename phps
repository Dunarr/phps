#!/bin/bash

if ! [[ "$1" =~ ^[0-9]+$ ]]; then
  echo "Erreur : Le premier paramètre doit être un nombre."
  exit 1
fi

phpversion="${1:0:1}.${1:1:1}"
current_user=$(whoami)
shift
rest_args="$@"

if docker images | grep -q "phps-$phpversion-fpm"; then
    docker run --rm --interactive --tty \
    --name=phps-"$phpversion"-fpm \
    --network host \
    --volume $PWD:/app -v ~/.ssh:/home/"$current_user"/.ssh \
    phps-"$phpversion"-fpm $rest_args
else
  echo "Aucune image Docker trouvée avec le tag $phpversion-fpm. Construction de l'image..."
  docker build -t phps-"$phpversion"-fpm --build-arg PHP_VERSION="$phpversion" --build-arg USER="$current_user" /usr/share/phps-docker/.
  if [ $? -eq 0 ]; then
     docker run --rm --interactive --tty \
        --name=phps-"$phpversion"-fpm \
        --network host \
        --volume "$PWD":/app -v ~/.ssh:/home/"$current_user"/.ssh \
        phps-"$phpversion"-fpm $rest_args
  else
    echo "Échec de la construction de l'image Docker."
    exit 1
  fi
fi
