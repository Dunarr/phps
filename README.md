PHPS: A simple php script to execute version php in docker containers

### Installation

- Clone repository in the folder you want
- execute `sudo chmod +x ./phps && sudo ln -s your_folder_path/phps /usr/local/bin/phps && sudo ln -s your_folder_path/phps-docker /usr/share/phps-docker`

### Use cases examples
- `phps 81 -v` : to execute php version 8.1 with -v arguments
- `phps 81 php` : run php cli example `phps 81 php -S localhost:8000`
- `phps 81 composer install` : run composer with php version you want
- `phps 82 symfony serve` : run symfony server with php 8.2